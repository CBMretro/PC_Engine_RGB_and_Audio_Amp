# PC Engine RGB and Audio Amp

[![License: CERN-OHL-S-2.0](./images/licence-cern-ohl-s-2.0.svg)](https://codeberg.org/CBMretro/PC_Engine_RGB_and_Audio_Amp/src/branch/main/LICENSE)

![](./images/PCE_Amp.jpg "")
![](./images/PCE_Amp2.jpg "")
![](./images/PCE_Amp3.jpg "")

This mod replaces RF modulator in original white model PC Engine with Mega Drive 2 style Mini-Din-9 RGB-video and audio out.

The amp board has Voultar's THS7374 based RGB amp circuit and Mobius' audio amp circuit on the same board.

Stereo audio from HuC6280 chip comes directly without amplification to PCE expansion port and is very low volume by default. The amp board has a simple LMV358 opamp amplification circuit designed by Mobius that amplifies the audio to line level.

I also designed an adapter board for installing Mini-Din-9 connector to PC Engine and adapter for PC Engine expansion port for easy wiring of the amp.

Preassembled amp boards can be ordered from JLCPCB. Gerbers, BOM and component position file required for JLCPCB SMT assembly are in gerber directory.

I have the kit for sale on my [Webstore.](https://cbmretro.fi/product/pc-engine-rgb-and-audio-amp/)

## Schematic

![](./images/PCE_Amp_SCH.png "")

## Configuration

The amp can be configured with solder jumpers according to what kind of Mini-Din-9 to RGB SCART cable is used.

The common cheap unshielded Mega Drive 2 SCART cables that are all over eBay and Aliexpress have 75 ohm resistors and 220 uF capacitors installed on RGB and Sync lines. The amp board can be configured to disable onboard output resistors and capacitors if the cable already has those installed.

Mega Drive 2 SCART cables are wired like this.

![](./images/MD2_SCART_SCH.png "")

And the amp should be configured like this.

![](./images/PCE_Amp_Conf.png "")

There's one caveat when using these cheap MD2 SCART cables. 5V from Mini-Din-9 is connected to SCART pin 8 that sets Status & Aspect Ratio. Voltages for it are:

* 0-2V off
* +5-8V on/16:9
* +9.5-12V on/4:3

So if your display has aspect ratio setting like my Sony KV29K5E when you power on PC Engine the display goes to 16:9 mode and you have to manually set it to 4:3.

You can cut the wire inside the SCART connector going to pin 8 but the cheap MD2 cables have glued/molded SCART connector so it's not easy to get in. You can also just cut the pin 8 on the outside.

A proper way to deal with this would be to install a DC to DC converter inside SCART connector that converts the voltage from 5V to 12V. 

If you are going to make your own cable it should be wired like this. Pins 4, 5, 9, 13, 14, 17 and 18 on the SCART are also grounds. You can connect ground to any of these pins and have a functioning cable as they are connected to same ground on the display.

![](./images/SCART.png "")

And the amp should be configured like this.

![](./images/PCE_Amp_Conf2.png "")

LPF should be set to either disabled or enabled. It shouldn't be left floating. THS7374 video amplifier has a low pass filter that removes high frequency noise from the output. My PC Engine has some noise that's visible on flat dark areas on my big 29" CRT. LPF removes this noise so I keep it on.

If you notice jail bars on the image you should do a jail bar fix eplained [here.](https://wiki.console5.com/wiki/Hu6260#Jailbar_.2F_Vertical_Bar_Fix)

## Installation tips

Ground from PCE expansion port to Amp board can be left unconnected. Amp board gets ground through the shield of Mini-Din-9 connector that is connected to ground plane underneath.

I routed the wires through the hole where one of the solder taps of RF shield was soldered.

You can try to solder Mini-Din-9 connector shield to a hole underneath it. Use flux on the hole to make solder to stick better.

![](./images/PCE_Amp4.jpg "")

If you don't want to use the Mini-Din-9 adapter board for installing the connector there's an alternative way. Scrape of solder mask under the connector exposing the ground plane and solder the connector directly to ground plane.

See this [guide](https://mmmonkey.co.uk/pc-engine-rgb-mod/) for this method.